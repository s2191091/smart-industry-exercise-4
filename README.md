# Smart Industry Exercise 4

## Exercise: Data triplification
The goal of this exercise is to write a script that transforms an IoT dataset serialised as CSV into an RDF graph. This process is know as data triplification.

- Python to write your script
- The RDFLib library to help you programmatically write triples
- Open Power System Data, as the dataset to be transformed
- SAREF, as the ontology that provides the classes and properties to create your RDF graph

 
## Dataset
The Open Power System DataLinks to an external site. Household Data is an open dataset that contains measurements of solar power generation as well as electricity consumption (load) in a resolution up to a single device consumption.

It consists of data collected from 68 devices located in 11 different buildings in a German city, among which:
- 3 industrial buildings
- 2 public buildings
- 6 residential households located

For this assignment, you use should the file containing measurements every 60 minutes, entitled household_data_60min_singleindex.csv.
