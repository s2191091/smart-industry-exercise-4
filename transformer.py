import sys
import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import XSD

# Get the dataset path from the command-line arguments
dataset_path = sys.argv[1]

# Define namespaces
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")

# Initialize RDF graph
g = Graph()
g.bind("saref", SAREF)
g.bind("ex", EX)

# Function to create a cleaned entity name
def get_appliance_name(column_name):
    parts = column_name.split('_')
    return '_'.join(parts[3:])

# Read the dataset
data = pd.read_csv(dataset_path)

# Define a function to add data to the RDF graph
for index, row in data.iterrows():
    utc_timestamp = row['utc_timestamp']
    cet_cest_timestamp = row['cet_cest_timestamp']
    interpolated = row['interpolated']

    for column, value in row.items():
        if column not in ['utc_timestamp', 'cet_cest_timestamp', 'interpolated']:
            if pd.notna(value):  # Check if the value is not NaN
                meas = EX[f"Measurement/{column}/{index}"]
                property_name = get_appliance_name(column)
                prop = EX[f"Property/{property_name}"]

                g.add((meas, RDF.type, SAREF.Measurement))
                g.add((prop, RDF.type, SAREF.Property))
                g.add((meas, SAREF.relatesToProperty, prop))
                g.add((meas, SAREF.hasValue, Literal(value, datatype=XSD.decimal)))
                g.add((meas, SAREF.hasTimestamp, Literal(utc_timestamp, datatype=XSD.dateTime)))


# Serialize the graph in Turtle format
output_file = "graph.ttl"
g.serialize(destination=output_file, format='turtle')
print(f"RDF graph is serialized to {output_file}")
